function save_options() {
    var parse_button = parseInt(document.getElementById("parse_button").value);
    var solve_button = parseInt(document.getElementById("solve_button").value);
    var text_before = document.getElementById("text_before").value;
    var text_after = document.getElementById("text_after").value;
    var alert_miss = document.getElementById("alert_miss").checked;
    chrome.storage.local.set({
        "parse_button": parse_button,
        "solve_button": solve_button,
        "text_before": text_before,
        "text_after": text_after,
        "alert_miss": alert_miss
    }, function() {
        var status = document.getElementById("status");
        status.textContent = "Applied!";
        setTimeout(function() {
            status.textContent = "";
        }, 750);
    });
}

function restore_options() {
    chrome.storage.local.get({
        ccna_url: "unknown",
        parse_button: 67,
        solve_button: 67,
        text_before: "",
        text_after: "",
        alert_miss: true
    }, function(items) {
        document.getElementById("ccna_url").textContent = items.ccna_url;
        document.getElementById("parse_button").value = items.parse_button;
        document.getElementById("solve_button").value = items.solve_button;
        document.getElementById("text_before").value = items.text_before;
        document.getElementById("text_after").value = items.text_after;
        document.getElementById("alert_miss").checked = items.alert_miss;
    });
}

document.addEventListener("DOMContentLoaded", restore_options);
document.getElementById("applyButton").addEventListener("click", save_options);

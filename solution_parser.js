var parseButton = 0;

chrome.storage.local.get({
        parse_button: 67
    }, function(items) {
        parseButton = items.parse_button;
    });

window.onkeyup = function(e) {
    
    var key = e.keyCode ? e.keyCode : e.which;
    var qTable = {};
    
    if (key == parseButton) {
        var questions = document.getElementsByTagName("strong");
        var answers = [];
        var lastQ = 0;
        var matchMode = false;
        var c_el = 0;
        var newQuestionMode = false;
        for (var i=0, max=questions.length; i < max; i++) {
            var element = questions[i];
            
            if (i != 0 && isNum(questions[i-1].innerHTML) && !newQuestionMode) {
                var qu = questions[lastQ].innerHTML;
                //qu = cutQuotes(qu);
                qTable[qu] = answers;
                lastQ = i;
                answers = [];
                matchMode = false;
                c_el++;
            }
            
            if (i != 0 && newQuestionMode && isNewQuestion(element.innerHTML)) {
                var t_question = questions[lastQ].innerHTML;
                if (isNewQuestion(t_question)) {
                    t_question = cutNewQuestion(t_question);
                }
                //t_question = cutQuotes(t_question);
                qTable[t_question] = answers;
                lastQ = i;
                answers = [];
                matchMode = false;
            }
            
            if (isMatch(element.innerHTML) && !newQuestionMode) {
                matchMode = true;
            }
            
            if (checkNewQuestion(element.innerHTML)) {
                newQuestionMode = true;
            }
            
            if (i > lastQ && ! (isNum(element.innerHTML)) && !matchMode && !checkNewQuestion(element.innerHTML)) {
                var ansStr = element.innerHTML;
                if (isOther(ansStr)) {
                    ansStr = element.getElementsByTagName("*")[0].innerHTML;
                }
                ansStr = ansStr.substring(0, ansStr.length - 1);
                answers.push(ansStr);
            }
        }
        
        var last_question = questions[lastQ].innerHTML;
        if (newQuestionMode) {
            last_question = cutNewQuestion(last_question);
        }
        //last_question = cutQuotes(last_question);
        
        qTable[last_question] = answers;
        
        for (var key in qTable) {
            var newKey = cutQuotes(key);
            if (!(key === newKey)) {
                qTable[newKey] = qTable[key];
            }
        }
        
        for (var key in qTable) {
            cutQuotes(key);
        }
        console.log(qTable);
        
        
        var str = alertTarget(qTable);
        var cUrl = document.URL;
        chrome.storage.local.set({
            ccna_url: cUrl,
            correct_answers: qTable
        });
        
        var showc = JSON.stringify(qTable);
        console.log(showc);
        
        for (var key in qTable) {
            console.log(key);
        }
        console.log(c_el);
    }
}

function isNum(elem) {
    return (! elem.includes("*")) && elem.length < 20;
}

function isOther(elem) {
    return elem.charAt(0) == "<";
}

function isMatch(elem) {
    return !isNaN(elem.substring(0,1)) && elem.substring(0,10).includes("Match");
}

function checkNewQuestion(elem) {
    return elem === "NEW QUESTIONS";
}

function isNewQuestion(elem) {
    var num = elem.substring(0, elem.indexOf("."));
    return !isNaN(num) && !(elem.indexOf("*") == elem.length -1);
}

function cutNewQuestion(elem) {
    var dot = elem.indexOf(".");
    return elem.substring(dot + 1, elem.length);
}

function cutQuotes(elem) {
    var ret = elem;
    //alert(ret.charAt(0));
    if (ret.charAt(0) === ' ') {
        //alert(ret);
        ret = ret.substring(1, ret.length);
    }
    return ret;
}

function alertTarget(mapping) {
    var str = "";
    
    for (var key in mapping) {
        str += key + "\n";
        var arr = mapping[key];
        for (var i = 0, max = arr.length; i < max; i++) {
            str += arr[i] + "\n";
        }
    }
    return str;
}

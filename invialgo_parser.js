var parseButton = 0;

setInterval(function () {
    var a = jQuery("div[class*=default]").parent();
    a.find("+").remove();
    a.remove();
}, 1000);

chrome.storage.local.get({
    parse_button: 67
}, function (items) {
    parseButton = items.parse_button;
});

window.onkeyup = function (e) {

    var key = e.keyCode ? e.keyCode : e.which;
    var solutions = {};

    if (key == parseButton) {
        var elements = document.getElementsByClassName("entry-content")[0].childNodes;
        var lastQuestion = "";
        var currentAnswers = [];

        for (var elementI = 0; elementI < elements.length; elementI++) {
            var element = elements[elementI];
            if (element.tagName === "OL") {
                solutions[lastQuestion] = currentAnswers;
                currentAnswers = [];
                lastQuestion = element.firstElementChild.textContent;
            }

            if (element.tagName === "P" && element.hasChildNodes() && element.firstChild.tagName === "STRONG") {
                currentAnswers.push(element.firstElementChild.textContent);
            }
        }

        solutions[lastQuestion] = currentAnswers;


        chrome.storage.local.set({
            ccna_url: document.URL,
            correct_answers: solutions
        });
        console.log(solutions);
        alert("solutions parsed!");
    }
};

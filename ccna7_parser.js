var parseButton = 0;

chrome.storage.local.get({
        parse_button: 67
    }, function(items) {
        parseButton = items.parse_button;
    });

window.onkeyup = function(e) {
    
    var key = e.keyCode ? e.keyCode : e.which;
    var solutions = {};
    
    if (key == parseButton) {
        var elements = document.getElementById("heatmapthemead_ad-50").parentElement.children;
        
        for (var elementI = 0; elementI < elements.length; elementI++) {
            var element = elements[elementI];
            if ( element.tagName === "OL") {
                
                var listChildren = element.children;
                
                for (var liI = 0; liI < listChildren.length; liI++) {
                    
                    var li = listChildren[liI];
                
                    var question = "";
                    var answers = [];
                
                    var hChildren = li.children;
                    for (var headerI = 0; headerI < hChildren.length; headerI++) {
                    
                        var header = hChildren[headerI];
                    
                        if (header.tagName === "H3") {
                            question = header.innerHTML;
                        }
						
						if (header.tagName === "H4") {
							
							if (header.hasChildNodes() && header.firstChild.tagName === "STRONG") {
								question = header.firstChild.innerHTML;
							}
						}
                    
                        if (header.tagName === "UL") {
                        
                            var aChildren = header.children;
                            for (var answerEI = 0; answerEI < aChildren.length; answerEI++) {
                            
                                var answerE = aChildren[answerEI];
                            
                                if (answerE.hasChildNodes() && answerE.firstChild.tagName === "SPAN") {
                                    answers.push(answerE.firstChild.textContent);
                                }
                            }
                        }
                        solutions[question] = answers;
                    }
                }
            }
        }
        
        chrome.storage.local.set({
            ccna_url: document.URL,
            correct_answers: solutions
        });
        console.log(solutions);
        alert("solutions parsed!");
    }
}

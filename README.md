# CCNA Solver #


### How do I get set up? ###

* Clone this repository, the newest TAG is recommended
* Type 
```
#!URL

chrome://extensions
```
in you URL bar

* Enable the Developer Mode and load the extension (select the cloned directory) or simply drag and drop the extension file from the bin directory into that page

### Configuration ###

* You can specify which text should be inserted before and after the correct answers
* You can specify the keys you have to press for several actions, click [here](http://keycode.info/) to find out the keycodes
* You can decide if you want to be notified, when no answer could be found

### Usage ###

* Visit the [solution site](https://ccna7.com) or [the better solution site](http://www.invialgo.com/2015/)
* Choose the chapter you need
* Press the button you have specified in the config
* Now a message should appear, that the extensions parsed the answers
* Optional: check the link in the config
* Now you can close the solution site
* Start your chaptertest and now you should be able to see the correct answer, when you press the specified button
* Hint: only use the parse and solve button on the solution site/on the chaptertest questionfragment

### Additional Infos ###

* The usage of this extension is on your own risk
* I am not responsible, if the extension fails and/or you fail your cisco exams
* With the usage of this extension you agree, that you are not allowed to tell any teacher about this extension
* This extension is only tested with Chromium 55 64 Bit on Arch Linux, but should also work on other OS with the Chrome Browser
* Feel free to contribute to this extension or fork it.
* Feel free to create new issues.

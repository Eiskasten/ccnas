var correctAnswers = {};
var solveButton = 0;
var text_after = "";
var text_before = "";
var exb_str = "Refer to the exhibit.";
var alert_miss = true;
       
chrome.storage.local.get( {
    correct_answers: {},
    ccna_url: "waß i ned",
    solve_button: 67,
    text_before: "> ",
    text_after: "",
    alert_miss: true
    
}, function(items) {
    correctAnswers = items.correct_answers;
    solveButton = items.solve_button;
    text_after = items.text_after;
    text_before = items.text_before;
    alert_miss = items.alert_miss;
});
       
window.onkeyup = function(e) {
   var key = e.keyCode ? e.keyCode : e.which;

   if (key == solveButton) {
       
       var current_question = "";
       var answers = [];
       
       var elements = document.getElementsByClassName("ai-stem");
       for (var i = 0; i < elements.length; i++) {
           var currElement = elements[i];
           current_question = currElement.textContent;
           if (current_question.indexOf(exb_str) >= 0) {
               current_question = exb_str + current_question.split(exb_str)[1];
           }
           console.log(correctAnswers[current_question]);
       }
       
        elements = document.getElementsByClassName("ai-option");
        if (correctAnswers[current_question] != null && 'length' in correctAnswers[current_question]) {
            for (var i = 0; i < elements.length; i++) {
                var currElement = elements[i];
                currElement = prepareElem(currElement);
                answers.push(currElement);
                if (isCorrect(currElement, correctAnswers[current_question])) {
                    markAsCorrect(currElement);
                }
            }
        } else {
            if (alert_miss) {
                alert("no answer found!");
            }
        }
       
       var str = "";
       
       for (var i = 0; i < answers.length; i++) {
           str += answers[i].innerHTML + "\n";
       }
   }
}

function isCorrect(element, answers) {
    var ret = false;
    console.log(JSON.stringify(answers));
    console.log(answers);
    console.log(correctAnswers);
    for (var i = 0; i < answers.length; i++) {
        var ans = element.innerHTML;
        var c_ans = answers[i];
        if (ans != null && c_ans != null && (typeof ans.replace === 'function') && (typeof c_ans.replace === 'function')) {
            ans = ans.replace(/&nbsp;/g, " ");
            c_ans = c_ans.replace(/&nbsp;/g, " ");
            ans = ans.replace(/ /g, "");
            c_ans = c_ans.replace(/ /g, "");
            if (c_ans === ans) {
                ret = true;
            }
        }
    }
    return ret;
}

function markAsCorrect(element) {
    element.insertAdjacentText('afterbegin', text_before);
    element.insertAdjacentText('beforeend', text_after);
}

function prepareElem(elem) {
    var ret = elem;
    if (ret.childElementCount > 0) {
        ret = elem.firstChild;
    }
    return ret;
}
